package business

import (
	_ "github.com/mattn/go-sqlite3"
	"log"
)

type Purchase struct {
	Name   string
	Amount int
	Price  int
	Date   string
}

func AddPurchaseByBookName(bookname string, amount int, price int) {
	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("insert into purchases (id, bookid, amount, price, date) values (?,(SELECT id from books WHERE name=?),?,?,strftime('%Y-%m-%d %H-%M-%S','now'))")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(nil, bookname, amount, price)
	if err != nil {
		log.Fatal(err)
	}
	tx.Commit()
}

func GetAggregatedBookPurchasesAmount() (aggregatedPurchases map[string]int) {
	aggregatedPurchases = make(map[string]int)
	rows, err := db.Query("SELECT name, sum(amount) FROM purchases JOIN books ON purchases.bookid = books.id GROUP by bookid")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var name string
		var amount int
		err = rows.Scan(&name, &amount)
		aggregatedPurchases[name] = amount
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return aggregatedPurchases
}

func GetAggregatedBookPurchasesPrice() (aggregatedPurchases map[string]int) {
	aggregatedPurchases = make(map[string]int)
	rows, err := db.Query("SELECT name, sum(price) FROM purchases JOIN books ON purchases.bookid = books.id GROUP by bookid")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var name string
		var sumprice int
		err = rows.Scan(&name, &sumprice)
		aggregatedPurchases[name] = sumprice
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return aggregatedPurchases
}

func GetPurchasesList() (purchasesList []Purchase) {
	rows, err := db.Query("SELECT name, amount, price, date FROM purchases JOIN books ON purchases.bookid = books.id")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var name string
		var amount int
		var price int
		var date string
		err = rows.Scan(&name, &amount, &price, &date)
		purchasesList = append(purchasesList, Purchase{Name: name, Amount: amount, Price: price, Date: date})
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return purchasesList
}
