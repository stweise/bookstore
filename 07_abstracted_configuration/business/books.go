package business

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"os"
)

type Book struct {
	Name      string
	Author    string
	Listprice int
	Active    bool
}

var db *sql.DB

func OpenDB(DBNAME string) {

	dbExists := fileExists(DBNAME)
	if !dbExists {
		log.Fatal("database not found")
		os.Exit(1)
	}

	var err error
	db, err = sql.Open("sqlite3", DBNAME)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	_, err = db.Exec("PRAGMA foreign_keys = ON;")
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}

func CloseDB() {
	log.Println("Closing Database")
	db.Close()
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func GetBooksFromDB() (booklist []Book) {
	rows, err := db.Query("select id, name, author, listprice, active from books")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var id int
		var name string
		var author string
		var listprice int
		var active bool
		err = rows.Scan(&id, &name, &author, &listprice, &active)
		if err != nil {
			log.Fatal(err)
		}
		// shows database IDs as internal field for DB management
		// fmt.Println(id, name, author, getCurrencyStringFromDSI(listprice), active)
		booklist = append(booklist, Book{Name: name, Author: author, Listprice: listprice, Active: active})
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return booklist
}

func AddBookIntoDB(b Book) {
	books := GetBooksFromDB()
	for _, v := range books {
		if v.Name == b.Name {
			log.Println("Book already in DB")
			return
		}
	}
	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("insert into books(id, name, author, listprice, active) values(?, ?, ?, ?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(nil, b.Name, b.Author, b.Listprice, b.Active)
	if err != nil {
		log.Fatal(err)
	}
	tx.Commit()
}

func HideBookInInventory(bookTitle string) {
	books := GetBooksFromDB()
	for _, v := range books {
		if v.Name == bookTitle {

			if v.Active == true {
				tx, err := db.Begin()
				if err != nil {
					log.Fatal(err)
				}

				stmt, err := tx.Prepare("update books set active = false where name = ?")
				if err != nil {
					log.Fatal(err)
				}
				defer stmt.Close()
				_, err = stmt.Exec(bookTitle)
				if err != nil {
					log.Fatal(err)
				}
				tx.Commit()
			} else {
				log.Println("Book already inactive")
			}

		}
	}
}
