package main

import (
	"bookstore.com/stweise/bookstore/business"
	"fmt"
	"github.com/spf13/viper"
	"html/template"
	"net/http"
	"os"
	"os/signal"
	"sort"
	"strconv"
    "log"
	"syscall"
)

func getCurrencyStringFromDSI(price int) string {
	numberString := fmt.Sprintf("€%03d", price)
	return numberString[:len(numberString)-2] + "," + numberString[len(numberString)-2:]
}

type Offer struct {
	Title  string
	Author string
	Price  string
	Amount string
}

type PageData struct {
	Offers []Offer
}

var templateDir string

func getCatalog(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles(templateDir + "/layout.html"))
	books := business.GetBooksFromDB()
	purchases := business.GetAggregatedBookPurchasesAmount()
	sales := business.GetAggregatedBookSalesAmount()
	var myOffers []Offer
	for _, v := range books {
		if v.Active {
			myOffers = append(myOffers, Offer{Title: v.Name, Author: v.Author, Amount: strconv.Itoa(purchases[v.Name] - sales[v.Name]), Price: getCurrencyStringFromDSI(v.Listprice)})
		}
	}
	data := PageData{
		Offers: myOffers,
	}
	tmpl.Execute(w, data)
}

func main() {
	// Set the file name of the configurations file
	viper.SetConfigName("config.yml")

	// Set the path to look for the configurations file
	viper.AddConfigPath("/etc/bookstore/")
	viper.AddConfigPath(".")

	// Enable VIPER to read Environment Variables
	viper.AutomaticEnv()

	viper.SetConfigType("yml")

	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Error reading config file, %s\nTrying to continue with environment variables and defaults\n", err)
	}

	log.Printf("Opening database: %s\n", viper.GetString("DATABASE"))

	templateDir = viper.GetString("TEMPLATES")
	business.OpenDB(viper.GetString("DATABASE"))
	defer business.CloseDB()

	//book management
	newbook := business.Book{Name: "Anna Karenina", Author: "Leo Tolstoy", Listprice: 1799, Active: true}
	business.AddBookIntoDB(newbook)
	//	business.HideBookInInventory("The Great Gatsby")
	//
	//
	//	//balance
	//	fmt.Println("Balance")
	//	fmt.Println("---------------------------------------------")
	//	printBalance()
	//	fmt.Println("---------------------------------------------")
	//
	mux := http.NewServeMux()
	mux.HandleFunc("/catalog/", getCatalog)
	mux.HandleFunc("/purchase/", purchase)
	mux.HandleFunc("/sell/", sell)
	mux.HandleFunc("/balance/", balance)
    mux.Handle("/", http.FileServer(http.Dir("./public/")))
	server := &http.Server{Addr: ":8080", Handler: mux}
	go func() { server.ListenAndServe() }()
	end := gracefulShutdown()
	<-end
}

func gracefulShutdown() <-chan struct{} {
	end := make(chan struct{})
	s := make(chan os.Signal, 1)
	signal.Notify(s, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-s
		fmt.Println("Shutting down gracefully. On my own terms.")
		// clean up here
		close(end)
	}()
	return end
}

func purchase(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles(templateDir + "/purchase.html"))
	var pageBookData struct {
		Summary string
		Offers  []string
	}
	books := business.GetBooksFromDB()
	myOffers := make([]string, 0)
	for _, v := range books {
		if v.Active {
			myOffers = append(myOffers, v.Name)
		}
	}
	pageBookData.Offers = myOffers
	switch r.Method {
	case "GET":
		tmpl.Execute(w, pageBookData)
	case "POST":
		price, _ := strconv.Atoi(r.PostFormValue("price"))
		amount, _ := strconv.Atoi(r.PostFormValue("amount"))
		business.AddPurchaseByBookName(r.PostFormValue("name"), amount, price)
		fmt.Println("Item: ", r.PostFormValue("name"))
		fmt.Println("Amount: ", r.PostFormValue("amount"))
		fmt.Println("Price: ", r.PostFormValue("price"))
		pageBookData.Summary = "Purchase processed"
		tmpl.Execute(w, pageBookData)
	}
}

func sell(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles(templateDir + "/sell.html"))
	type BookItem struct {
		Name  string
		Price string
	}
	var pageBookData struct {
		Summary string
		Offers  []BookItem
	}
	books := business.GetBooksFromDB()
	myOffers := make([]BookItem, 0)
	for _, v := range books {
		if v.Active {
			myOffers = append(myOffers, BookItem{Name: v.Name, Price: getCurrencyStringFromDSI(v.Listprice)})
		}
	}
	pageBookData.Offers = myOffers
	switch r.Method {
	case "GET":
		tmpl.Execute(w, pageBookData)
	case "POST":
		bookName := r.PostFormValue("name")
		amount, _ := strconv.Atoi(r.PostFormValue("amount"))
		totalPrice := 0
		for _, v := range books {
			if v.Active {
				if v.Name == bookName {
					totalPrice = amount * v.Listprice
				}
			}
		}
		business.AddSaleByBookName(bookName, amount, totalPrice)
		pageBookData.Summary = "Sale processed, we have gotten " + getCurrencyStringFromDSI(totalPrice) + " from customer."
		tmpl.Execute(w, pageBookData)
	}
}

func balance(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles(templateDir + "/balance.html"))
	purchases := business.GetAggregatedBookPurchasesPrice()
	sales := business.GetAggregatedBookSalesPrice()
	type BalanceItem struct {
		Name    string
		SumPur  string
		SumSal  string
		Balance string
	}
	var pageBalanceData struct {
		BalanceItems []BalanceItem
		TotalBalance string
	}

	keys := make([]string, 0, len(purchases))
	for k := range purchases {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	balanceItems := make([]BalanceItem, 0)
	totalBalance := 0
	for _, k := range keys {
		balanceItems = append(balanceItems, BalanceItem{Name: k, SumPur: getCurrencyStringFromDSI(purchases[k]), SumSal: getCurrencyStringFromDSI(sales[k]), Balance: getCurrencyStringFromDSI(sales[k] - purchases[k])})
		totalBalance += sales[k] - purchases[k]
	}
	pageBalanceData.BalanceItems = balanceItems
	pageBalanceData.TotalBalance = getCurrencyStringFromDSI(totalBalance)
	tmpl.Execute(w, pageBalanceData)
}
