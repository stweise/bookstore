module bookstore.com/stweise/bookstore

go 1.15

require (
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/spf13/viper v1.7.1
)
