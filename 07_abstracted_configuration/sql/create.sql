create table books (id integer not null primary key, name text, author text, listprice integer, active boolean);
create table purchases (id integer not null primary key, bookid integer, amount integer, price integer, date text, FOREIGN KEY(bookid) REFERENCES books(id));
create table sales (id integer not null primary key, bookid integer, amount integer, price integer, date text, FOREIGN KEY(bookid) REFERENCES books(id));
