---
title: ""
date: 2021-02-21T20:52:12+01:00
draft: false
---
## Welcome to the classic book store

This book store is different from others, at it is optimized for readers of paper and first generation e-ink reading devices. We try to be as compatible and old tech friendly as possible in our layout and web presence. The word classic in our title is therefore a double pun on the word classic.

Every book purchased in our store includes an EPUB or MOBI transfer to your preferred e-ink reading device. 

We have a growing number of devices we support:
* 2007 Amazon Kindle
* 2007 Bookeen	Cybook Gen3
* 2008 Sony PRS 505
* 2008 Sony PRS 700
* 2009 Amazon Kindle 2
* 2009 Iriver Story


Please call us any time if you have questions about compatibility.
