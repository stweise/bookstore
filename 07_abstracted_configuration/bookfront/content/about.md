---
title: "About"
date: 2021-02-21T20:14:25+01:00
draft: false
---
Kontakt:

info@the-classic-bookstore.com

the-classic-bookstore.com

+491234567890

		The classic book store GmbH
		Wiesenstrasse 15
		08150 Oberwiesheim



Registergericht: Oberwiesenheim

Registernummer: HRB 666666

Vertretungsberechtigte Geschäftsführer: Dr. Steffen Weise

Umsatzsteuer-Identifikationsnummer nach § 27a UStG: DE777777777
