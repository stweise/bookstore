module bookstore.com/stweise/bookstore

go 1.15

require (
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/msbranco/goconfig v0.0.0-20160629072055-3189001257ce // indirect
)
