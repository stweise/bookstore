sqlite3 bookstore.db < sql/create.sql
sqlite3 bookstore.db < sql/books.sql
sqlite3 bookstore.db < sql/purchases.sql
sqlite3 bookstore.db < sql/sales.sql
mv bookstore.db cmd/bookstore_backend/
