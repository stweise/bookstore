---
title: "New in stock: The Great Gatsby"
date: 2021-02-21T21:07:06+01:00
draft: false
---

We are happy to announce the "The Great Gatsby" is in store.

The Great Gatsby is a 1925 novel by American writer F. Scott Fitzgerald. Set in the Jazz Age on Long Island, the novel depicts narrator Nick Carraway's interactions with mysterious millionaire Jay Gatsby and Gatsby's obsession to reunite with his former lover, Daisy Buchanan.

The novel was inspired by a youthful romance Fitzgerald had with a socialite, and by parties he attended on Long Island's North Shore in 1922. Following a move to the French Riviera, he completed a rough draft in 1924. After submitting the draft to editor Maxwell Perkins, the editor persuaded Fitzgerald to revise the work over the following winter. After his revisions, Fitzgerald was happy with the text, but remained ambivalent about the book's title and considered a number of alternatives. The final title that he was documented to have desired was Under the Red, White, and Blue. Fitzgerald was pleased with painter Francis Cugat's final cover design, and incorporated a visual element from the art into the novel.
