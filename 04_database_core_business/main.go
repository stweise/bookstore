package main

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"os"
	"sort"
)

const DBNAME string = "./bookstore.db"

var db *sql.DB

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func getCurrencyStringFromDSI(price int) string {
	numberString := fmt.Sprintf("€%03d", price)
	return numberString[:len(numberString)-2] + "," + numberString[len(numberString)-2:]
}

func main() {

	dbExists := fileExists(DBNAME)
	if !dbExists {
		log.Fatal("database not found")
		os.Exit(1)
	}

	var err error
	db, err = sql.Open("sqlite3", DBNAME)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	defer db.Close()

	//book management
	newbook := book{name: "Anna Karenina", author: "Leo Tolstoy", listprice: 1799, active: true}
	addBookIntoDB(newbook)
	hideBookInInventory("The Great Gatsby")
	books := getBooksFromDB()
	fmt.Println("Booklist")
	fmt.Println("---------------------------------------------")
	for _, v := range books {
		if v.active {
			fmt.Println("Title: ", v.name, "\nAuthor: ", v.author, "\nListprice: ", getCurrencyStringFromDSI(v.listprice), "\n-------------------")
		}
	}
	fmt.Println("---------------------------------------------")

	//purchase
	addPurchaseByBookName(newbook.name, 10, 200)
	purchasesList := getPurchasesList()
	fmt.Println("Purchases")
	fmt.Println("---------------------------------------------")
	for _, v := range purchasesList {
		fmt.Printf("Purchase | T: %40s | amount: %4d | price:%8s | date: %s\n", v.name, v.amount, getCurrencyStringFromDSI(v.price), v.date)
	}
	fmt.Println("---------------------------------------------")

	//sale
	addSaleByBookName(newbook.name, 1, 1799)
	salesList := getSalesList()
	fmt.Println("Sales")
	fmt.Println("---------------------------------------------")
	for _, v := range salesList {
		fmt.Printf("Sale     | T: %40s | amount: %4d | price:%8s | date: %s\n", v.name, v.amount, getCurrencyStringFromDSI(v.price), v.date)
	}
	fmt.Println("---------------------------------------------")

	//balance
	fmt.Println("Balance")
	fmt.Println("---------------------------------------------")
	printBalance()
	fmt.Println("---------------------------------------------")

	//inventory
	fmt.Println("Inventory")
	fmt.Println("---------------------------------------------")
	printInventory()
	fmt.Println("---------------------------------------------")
}

func printBalance() {
	purchases := getAggregatedBookPurchasesPrice()
	sales := getAggregatedBookSalesPrice()

	keys := make([]string, 0, len(purchases))
	for k := range purchases {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	totalBalance := 0
	for _, k := range keys {
		fmt.Printf("SumPur   | T: %40s | price: %6s\n", k, getCurrencyStringFromDSI(purchases[k]))
		fmt.Printf("SumSal   | T: %40s | price: %6s\n", k, getCurrencyStringFromDSI(sales[k]))
		fmt.Printf("Balance  | T: %40s | price: %6s\n", k, getCurrencyStringFromDSI(sales[k]-purchases[k]))
		totalBalance += sales[k] - purchases[k]
	}
	fmt.Println("---------------------------------------------")
	fmt.Printf("TotalBalance %6s\n", getCurrencyStringFromDSI(totalBalance))
}

func printInventory() {
	// get all purchases
	purchases := getAggregatedBookPurchasesAmount()
	sales := getAggregatedBookSalesAmount()

	keys := make([]string, 0, len(purchases))
	for k := range purchases {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		fmt.Printf("Inventory| T: %40s | amount: %4d\n", k, purchases[k]-sales[k])
	}
}
