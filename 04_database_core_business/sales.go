package main

import (
	_ "github.com/mattn/go-sqlite3"
	"log"
)

type sale struct {
	name   string
	amount int
	price  int
	date   string
}

func addSaleByBookName(bookname string, amount int, price int) {
	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("insert into sales (id, bookid, amount, price, date) values (?,(SELECT id from books WHERE name=?),?,?,strftime('%Y-%m-%d %H-%M-%S','now'))")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(nil, bookname, amount, price)
	if err != nil {
		log.Fatal(err)
	}
	tx.Commit()
}

func getAggregatedBookSalesAmount() (aggregatedSales map[string]int) {
	aggregatedSales = make(map[string]int)
	rows, err := db.Query("SELECT name, sum(amount) FROM sales JOIN books ON sales.bookid = books.id GROUP by bookid")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var name string
		var amount int
		err = rows.Scan(&name, &amount)
		aggregatedSales[name] = amount
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return aggregatedSales
}

func getAggregatedBookSalesPrice() (aggregatedSales map[string]int) {
	aggregatedSales = make(map[string]int)
	rows, err := db.Query("SELECT name, sum(price) FROM sales JOIN books ON sales.bookid = books.id GROUP by bookid")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var name string
		var sumprice int
		err = rows.Scan(&name, &sumprice)
		aggregatedSales[name] = sumprice
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return aggregatedSales
}

func getSalesList() (salesList []sale) {
	rows, err := db.Query("SELECT name, amount, price, date FROM sales JOIN books ON sales.bookid = books.id")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var name string
		var amount int
		var price int
		var date string
		err = rows.Scan(&name, &amount, &price, &date)
		salesList = append(salesList, sale{name: name, amount: amount, price: price, date: date})
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return salesList
}
