package main

import (
	_ "github.com/mattn/go-sqlite3"
	"log"
)

type book struct {
	name      string
	author    string
	listprice int
	active    bool
}

func getBooksFromDB() (booklist []book) {
	rows, err := db.Query("select id, name, author, listprice, active from books")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		var id int
		var name string
		var author string
		var listprice int
		var active bool
		err = rows.Scan(&id, &name, &author, &listprice, &active)
		if err != nil {
			log.Fatal(err)
		}
		// shows database IDs as internal field for DB management
		// fmt.Println(id, name, author, getCurrencyStringFromDSI(listprice), active)
		booklist = append(booklist, book{name: name, author: author, listprice: listprice, active: active})
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return booklist
}

func addBookIntoDB(b book) {
	books := getBooksFromDB()
	for _, v := range books {
		if v.name == b.name {
			log.Println("Book already in DB")
			return
		}
	}
	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("insert into books(id, name, author, listprice, active) values(?, ?, ?, ?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()
	_, err = stmt.Exec(nil, b.name, b.author, b.listprice, b.active)
	if err != nil {
		log.Fatal(err)
	}
	tx.Commit()
}

func hideBookInInventory(bookTitle string) {
	books := getBooksFromDB()
	for _, v := range books {
		if v.name == bookTitle {

			if v.active == true {
				tx, err := db.Begin()
				if err != nil {
					log.Fatal(err)
				}

				stmt, err := tx.Prepare("update books set active = false where name = ?")
				if err != nil {
					log.Fatal(err)
				}
				defer stmt.Close()
				_, err = stmt.Exec(bookTitle)
				if err != nil {
					log.Fatal(err)
				}
				tx.Commit()
			} else {
				log.Println("Book already inactive")
			}

		}
	}
}
